package = 'OpenMWLuaDocumentor'
version = '0.2.0-1'
description = {
  summary = 'Fork of LuaDocumentor with some openmw-specific hacks',
  detailed = [[
    This is an example for the LuaRocks tutorial.
    Here we would put a detailed, typically
    paragraph-long description.
  ]],
  homepage = 'https://gitlab.com/ptmikheev/openmw-luadocumentor',
  license = 'EPL'
}
source = {
  url = 'git+https://gitlab.com/ptmikheev/openmw-luadocumentor'
}
dependencies = {
  'lua ~> 5.1',
  'luafilesystem ~> 1.6',
  'markdown ~> 0.32',
  'metalua-compiler ~> 0.7',
  'penlight ~> 0.9',
  'checks',
}
build = {
  type = 'builtin',
  install = {
    bin = {
      openmwluadocumentor = 'luadocumentor.lua'
    },
    lua = {
      ['models.internalmodelbuilder'] = 'models/internalmodelbuilder.mlua'
    }
  },
  modules = {
    defaultcss = 'defaultcss.lua',
    docgenerator = 'docgenerator.lua',
    extractors = 'extractors.lua',
    lddextractor = 'lddextractor.lua',
    templateengine = 'templateengine.lua',
    tealgenerator = 'tealgenerator.lua',

    ['fs.lfs'] = 'fs/lfs.lua',

    ['models.apimodel'] = 'models/apimodel.lua',
    ['models.apimodelbuilder'] = 'models/apimodelbuilder.lua',
    ['models.internalmodel'] = 'models/internalmodel.lua',
    ['models.ldparser'] = 'models/ldparser.lua',

    ['template.file'] = 'template/file.lua',
    ['template.functiontypedef'] = 'template/functiontypedef.lua',
    ['template.index'] = 'template/index.lua',
    ['template.index.recordtypedef'] = 'template/index/recordtypedef.lua',
    ['template.item'] = 'template/item.lua',
    ['template.page'] = 'template/page.lua',
    ['template.recordtypedef'] = 'template/recordtypedef.lua',
    ['template.usage'] = 'template/usage.lua',
    ['template.utils'] = 'template/utils.lua',
  }
}

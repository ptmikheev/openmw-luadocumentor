local lddextractor = require('lddextractor')

local typeNameSubstitutes = {
    [''] = 'any',
    ['unknown'] = 'any'
}

local metamethods = {
    __add = true,
    __sub = true,
    __mul = true,
    __div = true,
    __mod = true,
    __pow = true,
    __unm = true,
    __idiv = true,
    __band = true,
    __bor = true,
    __bxor = true,
    __bnot = true,
    __shl = true,
    __shr = true,
    __concat = true,
    __len = true,
    __eq = true,
    __lt = true,
    __le = true,
    __index = true,
    __newindex = true,
    __call = true,
}

local function valuesSortedByName(map)
    local result = {}
    for _, v in pairs(map) do
        table.insert(result, v)
    end
    table.sort(result, function(a, b) return a.name < b.name end)
    return result
end

local indentStyle = '  '

local function moduleName(path)
    -- expect packages to always be mentioned by the same path
    return path:gsub('[^%a]', '_')
end

local TealModule = {
    __tostring = function(self)
        return table.concat(self.__written)
    end,
}
TealModule.__index = {
    write = function(self, ...)
        local t = self.__written
        local lastText = t[#t] or '\n'
        local lineStart = lastText:sub(#lastText) == '\n'
        for _, s in ipairs({ ... }) do
            if #s > 0 then
                local anchor = 1
                while anchor <= #s do
                    local lineEnd = s:find('\n', anchor) or #s
                    if lineStart then
                        table.insert(t, self.__indent)
                    end
                    table.insert(t, s:sub(anchor, lineEnd))
                    lineStart = s:sub(lineEnd, lineEnd) == '\n'
                    anchor = lineEnd + 1
                end
            end
        end
    end,
    pushScope = function(self)
        self.__indent = self.__indent .. indentStyle
    end,
    popScope = function(self)
        self.__indent = self.__indent:sub(#indentStyle + 1)
    end,
    type = function(self, name)
        return name and self.module.types[name]
    end,
    isFunction = function(_, type)
        if not type then
            return false
        elseif type.tag == 'functiontypedef' then
            return true
        end
    end,

    writeHeader = function(self, unit)
        local shortdescription = unit.shortdescription or ''
        local longDescription = unit.description or ''
        local usage = unit.metadata and unit.metadata.usage
        local usageDescription = usage and usage[1] and usage[1].description or ''
        local empty = (#shortdescription + #longDescription + #usageDescription) == 0
        if empty then return end
        self:write('--[[\n')
        self:pushScope()
        if #shortdescription > 0 then
            self:write(shortdescription, '\n')
        end
        if #longDescription > 0 then
            self:write(longDescription, '\n')
        end
        if #usageDescription > 0 then
            self:write(usageDescription, '\n')
        end
        self:popScope()
        self:write(']]\n')
    end,
    -- must be at the end of the line
    writeNote = function(self, unit, printName)
        local name = printName and unit.name or ''
        local desc = unit.description or ''
        if #name + #desc == 0 then return end
        self:write(' --[[')
        if #name > 0 then
            self:write(name)
        end
        self:write(desc)
        self:write(']]')
    end,
    writeReference = function(self, ref, parent)
        local name = ref and (ref.typename or (ref.def and ref.def.name)) or ''
        local substitute = typeNameSubstitutes[name]
        if substitute then
            self:write(substitute)
            return
        end

        if name == '' then
            error('Empty name')
        end

        local typeT = self:type(name)
        if not typeT then
            if ref.modulename then
                local requiredName = moduleName(ref.modulename)
                -- equal type and modulename mean we are referencing the module itself
                if ref.modulename == name then
                    self:write(requiredName)
                else
                    self:write(requiredName, '.', name)
                end
            elseif ref.def and ref.def.structurekind then
                if ref.def.structurekind == 'list' then
                    self:write('{ ')
                    self:writeReference(ref.def.defaultvaluetyperef)
                    self:write(' }')
                elseif ref.def.structurekind == 'map' then
                    self:write('{ ')
                    self:writeReference(ref.def.defaulkeytyperef)
                    self:write(': ')
                    self:writeReference(ref.def.defaultvaluetyperef)
                    self:write(' }')
                else
                    error('Unknown structure kind ', ref.def.structurekind)
                end
            else
                self:write(name)
            end
        elseif typeT.tag == 'recordtypedef' then
            if not self.promotedTypes[typeT] then
                self:write(self.moduleType.name, '.')
            end
            self:write(name)
        elseif typeT.tag == 'functiontypedef' then
            self:writeFunction(typeT, parent)
        else
            error(table.concat { 'Unknown type declaration tag: "', typeT.tag, '" (', name, ')' })
        end
    end,
    writeReturns = function(self, unit)
        if #unit.returns == 0 then
            self:write('nil')
            return
        end
        local prev = nil
        self:pushScope()
        for _, ret in ipairs(unit.returns) do
            if prev then
                self:write(',')
                self:writeNote(prev, true)
                self:write('\n')
            end
            if #ret.types == 0 then
                self:write('any')
            end
            local firstType = true
            for _, ref in ipairs(ret.types) do
                if not firstType then
                    self:write(' | ')
                end
                self:writeReference(ref, unit)
                firstType = false
            end
            prev = ret
        end
        self:popScope()
    end,
    writeFunction = function(self, func, parent)
        self:write('function(')
        if #func.params > 0 then
            local prev = nil
            self:write('\n')
            self:pushScope()
            for _, param in ipairs(func.params) do
                if prev then
                    self:write(',')
                    self:writeNote(prev, false)
                    self:write('\n')
                end
                prev = param
                self:write(param.name, ': ')
                if not param.type and param.name == 'self' and parent then
                    self:writeReference({ typename = parent.name }, parent)
                else
                    self:writeReference(param.type, parent)
                end
            end
            self:popScope()
            self:write('\n')
        end
        self:write(')')

        if #func.returns > 0 then
            self:write(': ')
            self:writeReturns(func)
        end
    end,
    writeVariable = function(self, var, parent)
        self:writeHeader(var)
        if not parent then
            self:write('global ')
        end
        if metamethods[var.name] and self:isFunction(self:type(var.type.typename)) then
            self:write('metamethod ')
        end
        self:write(var.name, ': ')
        self:writeReference(var.type, parent)
        self:write('\n')
    end,
    writeDeclaration = function(self, typeT)
        self:writeHeader(typeT)

        if self.promotedTypes[typeT] then
            self:write('global ')
        end

        self:write('record ', typeT.name, '\n')
        self:pushScope()

        if typeT.supertype then
            local isUserdata = typeT.supertype.typename == 'userdata' and typeT.supertype.tag == 'primitivetyperef'
            if isUserdata then
                self:write('userdata\n')
            end
        end

        -- TODO: support supertypes?
        if typeT.structurekind == 'list' then
            local value = typeT.defaultvaluetyperef
            -- arrayrecord
            self:write('{ ')
            self:writeReference(value, typeT)
            self:write(' }\n')
        elseif typeT.structurekind == 'map' then
            local key = typeT.defaultkeytyperef
            local value = typeT.defaultvaluetyperef
            -- no maprecords yet, use a metamethod
            self:write('metamethod __index: function(self: ')
            self:writeReference({ typename = typeT.name })
            self:write(', index: ')
            self:writeReference(key, typeT)
            self:write('): ')
            self:writeReference(value, typeT)
            self:write('\n')
        elseif typeT.structurekind then
            error(table.concat { 'Unsupported structurekind: "', typeT.structurekind, '"' })
        end

        for _, field in ipairs(valuesSortedByName(typeT.fields)) do
            self:writeVariable(field, typeT)
            self:write('\n')
        end
        self:popScope()
        self:write('end\n')
    end,
    writeRequires = function(self)
        local moduleSet = {}
        local function requiredInUnits(refs)
            for _, ref in pairs(refs) do
                local refTypes = ref.types or { ref.type }
                for _, refType in ipairs(refTypes) do
                    local name = refType and refType.modulename
                    if name then
                        moduleSet[name] = true
                    end
                end
            end
        end

        for _, typeT in pairs(self.module.types) do
            if typeT.fields then
                requiredInUnits(typeT.fields)
            end
            if typeT.params then
                requiredInUnits(typeT.params)
            end
            if typeT.returns then
                requiredInUnits(typeT.returns)
            end
        end

        requiredInUnits(self.module.globalvars)

        local moduleList = {}
        for module, _ in pairs(moduleSet) do
            table.insert(moduleList, module)
        end
        table.sort(moduleList)
        for _, module in ipairs(moduleList) do
            self:write('local ', moduleName(module), ' = require("', module, '")\n')
        end
    end,
    generateDeclaration = function(self)
        local module = self.module
        local moduleRef = module:moduletyperef()
        local moduleType = moduleRef and self:type(moduleRef.typename)
        self.moduleType = moduleType

        self:writeHeader(module)
        self:write('\n')
        self:writeRequires()

        -- resolve conflicts between field names and child type names by promoting those types to global
        if moduleType then
            for _, field in pairs(moduleType.fields) do
                local typeName = field.type and field.type.typename
                local typeT = typeName and self:type(typeName)
                local notAnonymous = typeName and typeName:sub(1, 2) ~= '__'
                if typeT and notAnonymous then
                    self.promotedTypes[typeT] = true
                end
            end
            self.promotedTypes[moduleType] = true
        end

        local function writeType(typeT)
            local isAnonymous = typeT.name:sub(1, 2) == '__'
            if typeT ~= moduleType and not isAnonymous then
                self:writeDeclaration(typeT)
            end
        end

        local promotedList = {}
        for typeT in pairs(self.promotedTypes) do
            table.insert(promotedList, typeT)
        end
        promotedList = valuesSortedByName(promotedList)
        for _, typeT in ipairs(promotedList) do
            writeType(typeT)
        end

        if moduleType then
            self:write('local record ', moduleType.name, '\n')
            self:pushScope()
            for _, field in ipairs(valuesSortedByName(moduleType.fields)) do
                self:writeVariable(field, moduleType)
                self:write('\n')
            end
        end

        for _, typeT in ipairs(valuesSortedByName(module.types)) do
            if not self.promotedTypes[typeT] then
                writeType(typeT)
            end
        end

        if moduleType then
            self:popScope()
            self:write('end\n')
        end

        for _, variable in ipairs(valuesSortedByName(module.globalvars)) do
            self:writeVariable(variable)
        end

        self:write('return ')
        self:writeReturns(module)
    end,
}
TealModule.new = function(apiModule)
    return setmetatable({
        module = apiModule,
        __written = {},
        __indent = '',
        promotedTypes = {},
        moduleType = nil,
    }, TealModule)
end

local function generateforfiles(filenames)
    local definitions = {}
    local failedFiles = {}
    for _, filename in pairs(filenames) do
        local file, err = io.open(filename, 'r')
        if not file then return nil, 'Unable to read "' .. filename .. '"\n' .. err end
        local code = file:read('*all')
        file:close()

        local apimodule, err = lddextractor.generateapimodule(filename, code, false)
        if apimodule and apimodule.name then
            local tealModule = TealModule.new(apimodule)
            tealModule:generateDeclaration()
            definitions[filename] = tostring(tealModule)
        else
            table.insert(failedFiles, table.concat { 'Failed to parse "', filename, '" with "', err, '"' })
        end
    end
    return definitions, failedFiles
end

return {
    generateforfiles = generateforfiles,
}
